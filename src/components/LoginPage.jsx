import { Box, Button, Container, InputAdornment, TextField } from "@mui/material"
import PersonOutlineIcon from '@mui/icons-material/PersonOutline';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
const LoginPage = () => {
    return (
        <>
            <Container className="bg" maxWidth={"100%"}>
                <Box sx={{ display: 'flex', width: '50%', height:'100vh', backgroundColor:'#FFFFFF4A', margin:'-25px' }}>
                    <Box sx={{ display: 'flex', flexDirection: 'column', width: '50%', margin: 'auto' }}>
                        <h1 style={{ color: "#354446", fontSize: '60px' }}>Đăng nhập</h1>
                        <TextField variant="filled"
                            sx={{ borderBottom: "1px solid #354446" }}
                            label={"Tên tài khoản"}
                            InputLabelProps={{
                                style: {
                                    color: '#354446' // Set the label color here
                                }
                            }}
                            InputProps={{
                                style: {
                                    color: '#354446' // Set the font color here
                                },
                                endAdornment: (
                                    <InputAdornment position="start">
                                        <PersonOutlineIcon sx={{ color: "#354446" }} />
                                    </InputAdornment>
                                ),

                            }}

                        />
                        <TextField variant="filled"
                            label={"Mật khẩu"}
                            sx={{ marginTop: "20px", borderBottom: "1px solid #354446" }}
                            InputLabelProps={{
                                style: {
                                    color: '#354446' // Set the label color here
                                }
                            }}
                            InputProps={{
                                style: {
                                    color: '#354446' // Set the font color here
                                },
                                endAdornment: (
                                    <InputAdornment position="start">
                                        <VisibilityOffIcon sx={{ color: "#354446" }} />
                                    </InputAdornment>
                                ),
                            }}
                        />
                        <Button variant="contained" sx={{ background: "#354446", color: "white", marginTop: "35px", height: "45px", fontWeight: "700" }}>ĐĂNG NHẬP </Button>

                    </Box>

                </Box>
            </Container>
        </>
    )
}
export default LoginPage